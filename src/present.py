import sys
import os
import io
import traceback
import textwrap
import argparse
from contextlib import contextmanager
from cStringIO import StringIO


def _find_getch():
    try:
        import termios
    except ImportError:
        # Non-POSIX. Return msvcrt's (Windows') getch.
        import msvcrt
        return msvcrt.getch

    # POSIX system. Create and return a getch that manipulates the tty.
    import sys, tty
    def _getch():
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(fd)
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch

    return _getch

getch = _find_getch()


def getkey():
    """For special keys, return a unique string!"""
    ch = getch()
    if ch in ('\x00', '\xe0'):
        ch2 = getch()
        return '%02x:%02x' % (ord(ch), ord(ch2))
    else:
        return ch


def clear_screen():
    os.system('cls' if os.name == 'nt' else 'clear')


class Key(object):
    SPACE = ' '
    F1 = '00:3b'
    F10 = '00:44'
    SHIFT_F1 = '00:54'
    CTRL_F5 = '00:62'
    CTRL_C = '\x03'
    ESC = '\x1b'
    ENTER = '\r'
    BACKSPACE = '\x08'
    LEFT = 'e0:4b'
    NP_LEFT = '00:4b'
    RIGHT = 'e0:4d'
    NP_RIGHT = '00:4d'
    UP = 'e0:48'
    NP_UP = '00:48'
    DOWN = 'e0:50'
    NP_DOWN = '00:50'
    HOME = 'e0:47'
    NP_HOME = '00:47'
    END = 'e0:4f'
    NP_END = '00:4f'


class RestartPresentation(Exception):
    pass


class Executor(object):
    def __init__(self, scope=None):
        """The scope is used as both globals and locals

        because on module level, globals() is locals()!!

        When a variable is defined in module, it goes to the locals,
        but in order to be available in the functions and classes,
        it has to be in globals.
        """
        self.scope = scope or {}
        self._interactive_buffer = []
        self._future_features = set()

    def print_prompt(self):
        print '>>>',

    def print_command(self, command):
        for i, line in enumerate(command.split('\n')):
            if i:
                print '...', line
            else:
                print line

    def execute_command(self, command, additional_prompt=None, forbid_exec=False):
        old_scope_names = set(self.scope)
        feature_flags = 0
        for feature in self._future_features:
            feature_flags |= feature.compiler_flag
        try:
            self.scope['_'] = eval(compile(command, '<string>', 'eval', feature_flags), self.scope, self.scope)
            if self.scope['_'] is not None:
                print self.scope['_']
        except SyntaxError as e:
            if forbid_exec:
                raise
            if additional_prompt:
                print additional_prompt
            try:
                exec compile(command, '<string>', 'exec', feature_flags) in self.scope, self.scope
            except:
                self._print_traceback()
        except:
            self._print_traceback()
        for name in set(self.scope) - old_scope_names:
            scope_obj = self.scope[name]
            import __future__
            out, err = sys.stdout, sys.stderr
            try:
                sys.stdout, sys.stderr = StringIO(), StringIO()
                if isinstance(scope_obj, __future__._Feature):
                    self._future_features.add(scope_obj)
            finally:
                sys.stdout, sys.stderr = out, err

    def _print_traceback(self):
        typ, val, tb = sys.exc_info()
        print 'Traceback (most recent call last):'
        for filename, lineno, functionname, line in traceback.extract_tb(tb)[1:][-20:]:
            print '  File "%s", line %s, in %s' % (filename, lineno, functionname)
            if line:
                print '    %s' % line.strip()
        print '%s: %s' % (typ.__name__, val)

    def execute_interactively(self, commands, wait_before_exit=False):
        n = 0 # number of lines to execute before pausing
        try:
            for command in commands:
                if not command:
                    self.print_prompt()
                    print
                    continue
                self.print_prompt()
                if not n:
                    n = self._wait_for_user()
                n -= 1
                self.print_command(command)
                if not n:
                    n = self._wait_for_user(command)
                n -= 1
                self.execute_command(command, '...')
                for line in command.split('\n'):
                    self._interactive_buffer.append(line)
        except KeyboardInterrupt, e:
            print
            print 'exiting', e
        else:
            if wait_before_exit:
                print
                print
                print ' the end '.center(80, '-')
                print
                if getkey() != 'q':
                    print 'Finished. Press q to exit.'
                while getkey() != 'q':
                    pass

    def _interact(self):
        print
        lines = []
        while True:
            line = self._input_line(u'  \xbb ' if lines else u'\xbb\xbb\xbb ')
            if line is None:
                return
            self._interactive_buffer.append(line)
            if not line:
                if lines:
                    code = '\n'.join(lines)
                    self.execute_command(code)
                    lines = []
                continue
            if not lines:
                try:
                    self.execute_command(line, forbid_exec=True)
                except SyntaxError:
                    lines.append(line)
                except:
                    self._print_traceback()
            else:
                lines.append(line)
        print

    def _input_line(self, prompt):
        line_buffer = list(self._interactive_buffer)
        line_buffer.append('')
        line_number = len(line_buffer) - 1
        position_in_line = 0

        line = line_buffer[line_number]

        def write_current_line():
            sys.stdout.write(line)
            if position_in_line < len(line):
                sys.stdout.write((len(line) - position_in_line) * '\x08')

        def delete_current_line(prompt_too=False):
            sys.stdout.write(position_in_line * '\x08')
            sys.stdout.write(len(line) * ' ')
            sys.stdout.write(len(line) * '\x08')
            if prompt_too:
                sys.stdout.write(len(prompt) * '\x08 \x08')

        sys.stdout.write(prompt)
        k = getkey()
        while k != Key.ESC:
            if k == Key.F1:
                print
                print 'Interactive mode ON. Press ESC to exit interactive mode.'
                sys.stdout.write(prompt)
                write_current_line()
            if k == Key.CTRL_C:
                raise KeyboardInterrupt
            elif k == Key.CTRL_F5:
                raise RestartPresentation
            elif k == Key.ENTER:
                sys.stdout.write('\n')
                return line
            elif k == Key.BACKSPACE:
                if position_in_line > 0:
                    if position_in_line % 4 == 0 and line[position_in_line-4:position_in_line] == '    ':
                        deletion = 4
                    else:
                        deletion = 1
                    delete_current_line()
                    position_in_line -= deletion
                    line = line[:position_in_line] + line[position_in_line+deletion:]
                    write_current_line()
            elif k in (Key.UP, Key.NP_UP):
                if line_number > 0:
                    if line_number == len(line_buffer) - 1:
                        line_buffer[-1] = line
                    delete_current_line()
                    line_number -= 1
                    line = line_buffer[line_number]
                    position_in_line = len(line)
                    write_current_line()
            elif k in (Key.DOWN, Key.NP_DOWN):
                if line_number < len(line_buffer) - 1:
                    delete_current_line()
                    line_number += 1
                    line = line_buffer[line_number]
                    position_in_line = len(line)
                    write_current_line()
            elif k in (Key.LEFT, Key.NP_LEFT):
                if position_in_line > 0:
                    position_in_line -= 1
                    sys.stdout.write('\x08')
            elif k in (Key.HOME, Key.NP_HOME):
                while position_in_line > 0:
                    position_in_line -= 1
                    sys.stdout.write('\x08')
            elif k in (Key.RIGHT, Key.NP_RIGHT):
                if position_in_line < len(line):
                    sys.stdout.write(line[position_in_line])
                    position_in_line += 1
            elif k in (Key.END, Key.NP_END):
                while position_in_line < len(line):
                    sys.stdout.write(line[position_in_line])
                    position_in_line += 1
            elif isinstance(k, str) and len(k) == 1:
                if k == '\t':
                    next_position_in_line = (position_in_line // 4) * 4 + 4
                    insertion = (next_position_in_line - position_in_line) * ' '
                else:
                    insertion = unicode(k, sys.stdin.encoding)
                line = line[:position_in_line] + insertion + line[position_in_line:]
                sys.stdout.write(line[position_in_line:])
                position_in_line += len(insertion)
                if position_in_line < len(line):
                    sys.stdout.write((len(line) - position_in_line) * '\x08')
            k = getkey()

        delete_current_line(prompt_too=True)

    def _wait_for_user(self, printed_command=None):
        k = getkey()
        while k not in (Key.SPACE, Key.ENTER):
            if k == Key.ESC:
                self._interact()
                self.print_prompt()
                if printed_command:
                    self.print_command(printed_command)
            if k == Key.F1:
                print
                print 'Commands:'
                print 'SPACE:    execute next line'
                print 'ENTER:    execute next line'
                print 'F10:      execute next 10 or so lines'
                print 'ESC:      interactive mode'
                print 'F1:       show help (this)'
                print 'CTRL-F5:  restart presentation'
                print 'CTRL-C:   exit'
                print 'SHIFT-F1: (debugging) Inspect next keyboard key code'
                print
                self.print_prompt()
            if k == Key.F10:
                return 12
            if k == Key.SHIFT_F1:
                print
                print 'Inspect key:',
                k2 = getkey()
                print repr(k2)
                print
                self.print_prompt()
            elif k == Key.CTRL_C:
                raise KeyboardInterrupt
            elif k == Key.CTRL_F5:
                raise RestartPresentation
            k = getkey()
        return 1


def split_on_empty_line(script, additional_empty_lines=0):
    command = []
    for line in script.splitlines():
        line = line.rstrip()
        if line == '# coding: utf-8':
            continue

        if line and line[0] not in ' \t\n\r':
            import itertools
            empty_lines = list(itertools.takewhile(lambda commandline: not commandline, reversed(command)))
            if empty_lines:
                yield '\n'.join(command[:-len(empty_lines)])
                for i in range(len(empty_lines)-1):
                    yield ''
                for i in range(additional_empty_lines):
                    yield ''
                command = []

        command.append(line)

    if command:
        yield '\n'.join(command)


def parse_args(argv):
    parser = argparse.ArgumentParser(description='Execute python instructions as an interactive presentation.')
    parser.add_argument('script_path', metavar='SCRIPT', type=str, help='Script to execute. '
                        'This can be either path to a file or the name of a script in scripts directory. '
                        'The script will be split into blocks on empty lines and '
                        'executed block by block.')
    parser.add_argument('-o', metavar='FILENAME', type=str, help='Filename to store copy of stdout',
                        dest='storing_filename', default=None)
    parser.add_argument('-s', '--spaceous', dest='additional_empty_lines', default=0, action='store_const', const=1,
                        help='Add additional space between commands')
    parser.add_argument('--wait', dest='wait', default=False, action='store_true',
                        help='Do not exit when done. Wait for user to exit.')
    args = parser.parse_args()
    scripts_dir = os.path.join(os.path.dirname(__file__), 'scripts')
    if not os.path.exists(args.script_path):
        path2 = os.path.join(scripts_dir, '%s.py' % args.script_path)
        if os.path.exists(path2):
            args.script_path = path2
    return args


class StreamSplitter(object):
    def __init__(self, streams):
        self._streams = list(streams)

    def write(self, s):
        for stream in self._streams:
            stream.write(unicode(s))


@contextmanager
def output_storing(filename=None):
    if filename:
        with io.open(filename, 'at', encoding='utf-8') as f:
            old_stdout, sys.stdout = sys.stdout, StreamSplitter([sys.stdout, f])
            try:
                yield
            finally:
                sys.stdout = old_stdout
    else:
        yield


def main(argv):
    args = parse_args(argv)
    with output_storing(args.storing_filename):
        while True:
            try:
                with open(args.script_path) as f:
                    script = unicode(f.read(), 'utf-8')
                clear_screen()
                executor = Executor()
                commands = split_on_empty_line(script, additional_empty_lines=args.additional_empty_lines)
                executor.execute_interactively(commands, wait_before_exit=args.wait)
            except RestartPresentation: # CTRL-F5
                # Print a header before clearing screen, so that it remains in output file
                print
                print 80 * "*"
                print "Restarting presentation"
                print 80 * "*"
                print
                continue
            else:
                break


if __name__ == "__main__":
    main(sys.argv)

